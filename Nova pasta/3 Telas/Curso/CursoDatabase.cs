﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Telas.Curso
{
    public class CursoDatabase
    {
        public int salvar(CursoDTO dto)
        {

            string script =
                @" INSERT INTO tb_curso (nm_curso , nm_periodo , ds_turma, vl_mensalidade)
                               VALUES (@nm_curso , @nm_periodo , @ds_turma, @vl_mensalidade)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_curso", dto.Curso));
            parms.Add(new MySqlParameter("nm_periodo", dto.Perido));
            parms.Add(new MySqlParameter("ds_turma", dto.Turma));
            parms.Add(new MySqlParameter("vl_mensalidade", dto.Mensalidade));
          


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<CursoDTO> Listar()
        {
            string script = "select*from tb_curso";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CursoDTO> lista = new List<CursoDTO>();

            while (reader.Read())

            {
                CursoDTO dto = new CursoDTO();
                dto.ID_curso = reader.GetInt32("id_id_curso");
                dto.Curso = reader.GetString("nm_curso");
                dto.Perido = reader.GetString("nm_periodo");
                dto.Turma = reader.GetString("ds_turma");
                dto.Mensalidade = reader.GetDecimal("vl_mensalidade");
                

                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }
    }
}
