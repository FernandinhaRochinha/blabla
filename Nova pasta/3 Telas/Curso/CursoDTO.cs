﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Telas.Curso
{
    public class CursoDTO
    {
        public int ID_curso { get; set; }

        public string Turma { get; set; }

        public string Curso { get; set; }

        public string Perido { get; set; }

        public decimal  Mensalidade { get; set; }

    }
}
