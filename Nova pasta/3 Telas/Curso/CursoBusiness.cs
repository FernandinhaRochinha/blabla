﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Telas.Curso
{
    public class CursoBusiness
    {
        CursoDatabase db = new CursoDatabase();
        public List<CursoDTO> listar()
        {
           
            return db.Listar();

        }

        public int salvar(CursoDTO dto)
        {
            return db.salvar(dto);

        }
    }
}
