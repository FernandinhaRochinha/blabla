﻿using _3_Telas.BD;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Telas.Carnezinhyo
{
    class CarneBusiness
    {
        public int salvar(CarneDTO dto)
        {
            if (dto.Aluno == string.Empty)
            {
                throw new ArgumentException("nome é obrigatório");
            }
            if (dto.Turma == string.Empty)
            {
                throw new ArgumentException("turma é obrigatório");
            }
            if (dto.Curso == string.Empty)
            {
                throw new ArgumentException("curso é obrigatório");
            }
            if (dto.mensalidade == 0)
            {
                throw new ArgumentException("mensalidade é brigatório");
            }


            CarneDatabase db = new CarneDatabase();
            return db.salvar(dto);
        }
        public List<CarneDTO> listar()
        {
            CarneDatabase db = new CarneDatabase();
            return db.Listar();


        }

    }
    
}
