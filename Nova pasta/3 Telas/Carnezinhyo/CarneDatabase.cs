﻿using _3_Telas.BásicosCarne;
using MySql.Data.MySqlClient;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Telas.BD
{
    public class CarneDatabase
    {
        public int salvar(CarneDTO dto)
        {

            string script =
                @" INSERT INTO tb_aluno (nm_aluno , id_curso , ds_rg, ds_cpf , dt_nascimento)
                               VALUES (@nm_aluno , @id_curso , @ds_rg, @ds_cpf , @dt_nascimento)";

            List<MySqlParameter> parms = new List<MySqlParameter>();
            parms.Add(new MySqlParameter("nm_aluno", dto.Aluno));
            parms.Add(new MySqlParameter("id_curso", dto.Curso));
            parms.Add(new MySqlParameter("ds_rg", dto.Rg));
            parms.Add(new MySqlParameter("ds_cpf", dto.CPF));
            parms.Add(new MySqlParameter("dt_nascimento", dto.Nascimento));


            Database db = new Database();
            return db.ExecuteInsertScriptWithPk(script, parms);

        }
        public List<CarneDTO>Listar()
        {
            string script = "select*from tb_aluno";

            List<MySqlParameter> parms = new List<MySqlParameter>();

            Database db = new Database();
            MySqlDataReader reader = db.ExecuteSelectScript(script, parms);

            List<CarneDTO> lista = new List<CarneDTO>();

            while (reader.Read())

            {
                CarneDTO dto = new CarneDTO();
                dto.Id = reader.GetInt32("id_aluno");
                dto.Aluno = reader.GetString("nm_aluno");
                dto.Rg = reader.GetString("ds_rg");
                dto.Curso = reader.GetInt32("id_curso");
                dto.Nascimento = reader.GetDateTime("dt_nascimento");
                dto.CPF = reader.GetString("ds_cpf");

                lista.Add(dto);
            }
            reader.Close();
            return lista;


        }



    }   


        
}

