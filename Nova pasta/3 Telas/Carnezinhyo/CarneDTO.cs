﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace _3_Telas.BD
{
   public class CarneDTO
    {
        public int Id { get; set; }

        public string Aluno { get; set; }

        public string Rg { get; set; }

        public int Curso { get; set; }

        public string CPF{ get; set; }

        public DateTime Nascimento { get; set; }
    }
}
