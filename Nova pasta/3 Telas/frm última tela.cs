﻿using _3_Telas.BD;
using _3_Telas.Carnezinhyo;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace _3_Telas
{
    public partial class frm_última_tela : Form
    {
        public frm_última_tela()
        {
            InitializeComponent();

            CarneBusiness busines = new CarneBusiness();
            List<CarneDTO> listar = busines.listar();

           dgvcarne.AutoGenerateColumns = false;
           dgvcarne.DataSource = listar;

        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void frm_última_tela_Load(object sender, EventArgs e)
        {

        }

        private void button1_Click(object sender, EventArgs e)
        {
            txtmensalidade tela = new txtmensalidade();
            tela.Show();
            this.Close();
        }

        private void dgvcarne_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {

        }
    }
}
